class Challenge
  include Mongoid::Document
  field :title, type: String
  field :description, type: String
  field :author, type: String
end

